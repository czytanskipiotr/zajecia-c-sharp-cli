﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using _13II;

namespace _13IITests
{
    [TestClass]
    public class BasicTests
    {
        [DataTestMethod]
        [DataRow("101", 5)]
        [DataRow("110", 6)]
        [DataRow("000000000000", 0)]
        [DataRow("000000000001", 1)]
        public void BinaryToDec(string input, int output)
        {
            Assert.AreEqual(output, Basic.BinaryToDec(input));
        }

        [DataTestMethod]
        [DataRow("aaabbCdFgHiA", 9)]
        [DataRow("110", 2)]
        [DataRow("<>&^!!", 5)]
        public void DistinctCharactersTest(string input, int output)
        {
            Assert.AreEqual(output, Basic.DistinctCharacters(input));
        }
        [DataTestMethod]
        [DataRow("aaabbCdFgHiA", 3)]
        [DataRow("110", 1)]
        [DataRow("<>&^!!", 1)]
        public void RepetitionsNumberTest(string input, int output)
        {
            Assert.AreEqual(output, Basic.RepetitionsNumber(input));
        }
        [DataTestMethod]
        [DataRow(0, 1)]
        [DataRow(1, 1)]
        [DataRow(2, 2)]
        [DataRow(5, 120)]
        public void FactorialTest(int input, int output)
        {
            Assert.AreEqual(output, Basic.Factorial(input));
        }

        [DataTestMethod]
        [DataRow(0, 0)]
        [DataRow(1, 1)]
        [DataRow(4, 3)]
        [DataRow(17, 1597)]
        public void FibonacciTest(int input, int output)
        {
            Assert.AreEqual(output, Basic.Fibonacci(input));
        }


    }
}
