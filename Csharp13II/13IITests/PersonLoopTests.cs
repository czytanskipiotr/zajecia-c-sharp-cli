using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using _13II;

namespace _13IITests
{
    [TestClass]
    public class PersonLoopTests
    {
        [TestMethod]
        public void SimpleLoop()
        {
            var adam = new Person("Adam", "Kowalski");
            var jan = new Person("Jan", "Nowak");
            var personManager = new PersonManager();
           
            var adamId = personManager.AddPerson(adam);
            var janId = personManager.AddPerson(jan);
            //jan jest podwladnym adama
            personManager.SetBoss(janId, adamId);
            //adam nie moze byc podwladnym jana
            Assert.ThrowsException<ArgumentException>(()=>personManager.SetBoss(adamId, janId));
         }
        [TestMethod]
        public void FirstCannotBeSubordinateOfThird()
        {
            var adam = new Person("Adam", "Kowalski");
            var jan = new Person("Jan", "Nowak");
            var piotr = new Person("Piotr", "Iks");
            var personManager = new PersonManager();

            var adamId = personManager.AddPerson(adam);
            var janId = personManager.AddPerson(jan);
            var piotrId = personManager.AddPerson(piotr);
            //piotr jest podwladnym jana
            personManager.SetBoss(piotrId, janId);
            //jan jest podwladnym adama
            personManager.SetBoss(janId, adamId);
            //adam nie moze byc podwladnym piotra
            Assert.ThrowsException<ArgumentException>(() => personManager.SetBoss(adamId, piotrId));
        }
    }
}
