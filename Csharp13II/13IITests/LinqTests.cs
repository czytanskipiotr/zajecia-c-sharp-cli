using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using _13II;

namespace _13IITests
{
    [TestClass]
    public class LinqTests
    {
        [TestMethod]
        public void HigherSalaryTest()
        {
            var adam = new Person("Adam", "Kowalski",1262);
            var jan = new Person("Jan", "Nowak",6231);
            var piotr = new Person("Piotr", "Iks",5161);
            var personManager = new PersonManager();

           personManager.AddPerson(adam);
           personManager.AddPerson(jan);
           personManager.AddPerson(piotr);
           Assert.AreEqual(jan, personManager.GetHigherSalaryPerson());
        }

        [TestMethod]
        public void GetPersonsWithoutBoss()
        {
            var adam = new Person("Adam", "Kowalski", 1262);
            var jan = new Person("Jan", "Nowak", 6231);
            var piotr = new Person("Piotr", "Iks", 5161);
            var personManager = new PersonManager();

           var adamId = personManager.AddPerson(adam);
           var janId = personManager.AddPerson(jan);
           var piotrId = personManager.AddPerson(piotr);
            personManager.SetBoss(adamId, janId);
            var personsWithoutBoss = personManager.GetPersonsWithoutBoss().ToList();
            Assert.AreEqual(2, personsWithoutBoss.Count());
            Assert.IsTrue(personsWithoutBoss.Contains(jan));
            Assert.IsTrue(personsWithoutBoss.Contains(piotr));
        }
        [TestMethod]
        public void GetPersonsDelegateTest()
        {
            var adam = new Person("Adam", "Kowalski", 1262);
            var jan = new Person("Jan", "Nowak", 6231);
            var piotr = new Person("Piotr", "Iks", 5161);
            var personManager = new PersonManager();

            var adamId = personManager.AddPerson(adam);
            var janId = personManager.AddPerson(jan);
            var piotrId = personManager.AddPerson(piotr);
            personManager.SetBoss(adamId, janId);
            Assert.AreEqual(1, personManager.GetPersons(x=>x.Name=="Piotr").Count());
            Assert.AreEqual(1, personManager.GetPersons(x => x.GetSalary() == 1262).Count());
            Assert.AreEqual(adam, personManager.GetPersons(x => x.Boss==jan).First());
        }

        [TestMethod]
        public void LargestNumbeOfCoordinatesTest()
        {
            var adam = new Person("Adam", "Kowalski", 1262);
            var jan = new Person("Jan", "Nowak", 6231);
            var piotr = new Person("Piotr", "Iks", 5161);
            var ignacy = new Manager("Ignacy", "Sks", 1161);
            var personManager = new PersonManager();
            var adamId = personManager.AddPerson(adam);
            var janId = personManager.AddPerson(jan);
            var piotrId = personManager.AddPerson(piotr);
            var ignacyId = personManager.AddPerson(ignacy);

            personManager.SetBoss(adamId, janId);
            personManager.SetBoss(piotrId, janId);
            personManager.SetBoss(janId, ignacyId);

            var result = personManager.TheLargestNumberOfSubordinates().ToArray();
            Assert.AreEqual(jan, result[0].Item1);
            Assert.AreEqual(2, result[0].Item2);
            Assert.AreEqual(ignacy, result[1].Item1);
            Assert.AreEqual(1, result[1].Item2);
        }
        [TestMethod]
        public void GetIdsWithSalariesTest()
        {
            var adam = new Person("Adam", "Kowalski", 1262);
            var jan = new Person("Jan", "Nowak", 6231);
            var piotr = new Person("Piotr", "Iks", 5161);
            var personManager = new PersonManager();

            var adamId = personManager.AddPerson(adam);
            var janId = personManager.AddPerson(jan);
            var piotrId = personManager.AddPerson(piotr);

            var result = personManager.GetIdsWithSalaries().ToList();
            Assert.IsTrue(result.Contains(new Tuple<int, int>(adamId,1262)));
            Assert.IsTrue(result.Contains(new Tuple<int, int>(janId, 6231)));
            Assert.IsTrue(result.Contains(new Tuple<int, int>(piotrId, 5161)));
            Assert.AreEqual(3, result.Count);
        }
        [TestMethod]
        public void GetSumOfSalariesByParityIdTest()
        {
            var adam = new Person("Adam", "Kowalski", 1262);//id=0
            var jan = new Person("Jan", "Nowak", 6231);//id=1
            var piotr = new Person("Piotr", "Iks", 5161);//id=2
            var personManager = new PersonManager();

            var adamId = personManager.AddPerson(adam);
            var janId = personManager.AddPerson(jan);
            var piotrId = personManager.AddPerson(piotr);

            var result = personManager.GetSumOfSalariesByParity();
            Assert.AreEqual(1262+5161,result.First(x => x.Item1==0).Item2);
            Assert.AreEqual(6231, result.First(x => x.Item1 == 1).Item2);
        }
    }
}
