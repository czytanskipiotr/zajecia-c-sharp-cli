﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using _13II;

namespace _13IITests
{
    [TestClass]
    public class PersonTests
    {
        [TestMethod]
        public void BasicConstructorTest()
        {
            var person = new Person("Adam", "Kowalski");
            Assert.AreEqual("Adam", person.Name);
            Assert.AreEqual("Kowalski", person.LastName);

            var person2 = new Person("Jan", "Nowak", 1600);
            Assert.AreEqual("Jan",person2.Name);
            Assert.AreEqual("Nowak", person2.LastName);
            Assert.AreEqual(1600, person2.GetSalary());
        }
        [TestMethod]
        public void PersonSalaryTest()
        {
            var person = new Person("Adam", "Kowalski");
            person.SetSalary(1500);
            Assert.AreEqual(1500, person.GetSalary());
        }
        [TestMethod]
        public void ManagerSalaryTest()
        {
            var person = new Manager("Adam", "Kowalski");
            person.SetSalary(2000);
            Assert.AreEqual(2400, person.GetSalary());
        }
        [TestMethod]
        public void BossTest()
        {
            var person = new Person("Adam", "Kowalski");
            var person2 = new Person("Jan", "Nowak");
            person.AddSubordinate(person2);
            person2.Boss = person;
            Assert.AreEqual("Adam", person2.Boss.Name);
            Assert.AreEqual(1, person.GetSubordinates().Count);
        }
        [TestMethod]
        public void PersonManagerAddAndGet()
        {
            var adam = new Person("Adam", "Kowalski");
            var jan = new Person("Jan", "Nowak");
            var personManager = new PersonManager();
            var countBefore = personManager.Counter;
            var adamId = personManager.AddPerson(adam);
            var janId = personManager.AddPerson(jan);
            var countAfter = personManager.Counter;
            personManager.SetBoss(janId, adamId);
            Assert.AreEqual("Adam", jan.Boss.Name);
            Assert.AreEqual(1, adam.GetSubordinates().Count);
            Assert.AreEqual(0, countBefore);
            Assert.AreEqual(2, countAfter);
        }
        [TestMethod]
        public void PersonManagerDelete()
        {
            var adam = new Person("Adam", "Kowalski");
            var jan = new Person("Jan", "Nowak");
            var personManager = new PersonManager();
            var countBefore = personManager.Counter;
            var adamId = personManager.AddPerson(adam);
            var janId = personManager.AddPerson(jan);
            var countAfter = personManager.Counter;
            personManager.SetBoss(janId, adamId);
            personManager.RemovePerson(janId);
            Assert.AreEqual(1, personManager.Counter);
        }
        [TestMethod]
        public void PersonManagerDeleteBoss()
        {
            var adam = new Person("Adam", "Kowalski");
            var jan = new Person("Jan", "Nowak");

            var personManager = new PersonManager();
          
            var adamId = personManager.AddPerson(adam);
            var janId = personManager.AddPerson(jan);
         
            personManager.SetBoss(janId, adamId);
            personManager.RemovePerson(adamId);

            Assert.AreEqual(null, jan.Boss);
        }
        [TestMethod]
        public void PersonManagerDeleteBossInTheMiddle()
        {
            var pierwszy = new Person("Adam", "Kowalski");
            var drugi = new Person("Jan", "Nowak");
            var trzeci = new Person("Piotr", "Piotrowski");

            var personManager = new PersonManager();

            var pierwszyId = personManager.AddPerson(pierwszy);
            var drugiId = personManager.AddPerson(drugi);
            var trzeciId = personManager.AddPerson(trzeci);
            //pierwszy jest szefem drugiego, drugi trzeciego
            personManager.SetBoss(drugiId, pierwszyId);
            personManager.SetBoss(trzeciId, drugiId);

            Assert.AreEqual("Adam", drugi.Boss.Name);
            Assert.AreEqual("Jan", trzeci.Boss.Name);

            Assert.AreEqual("Jan", pierwszy.GetSubordinates()[0].Name);
            Assert.AreEqual(1, pierwszy.GetSubordinates().Count);
            Assert.AreEqual(1, drugi.GetSubordinates().Count);
            personManager.RemovePerson(drugiId);

            Assert.AreEqual("Adam", trzeci.Boss.Name);
            Assert.AreEqual("Piotr", pierwszy.GetSubordinates()[0].Name);

           
        }
    }
}
