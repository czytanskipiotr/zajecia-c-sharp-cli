﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13II
{
    public class Person
    {
        protected string _name;
        protected string _lastName;
        protected int _salary;
        private Person _boss;
        private List<Person> _subordinates = new List<Person>();

        public Person(string name, string lastname)
        {
            _name = name;
            _lastName = lastname;
          
        }

        public Person( string name, string lastname, int salary)
        {
            _name = name;
            _lastName = lastname;
            _salary = salary;
        }

        public bool AddSubordinate(Person subordinate)
        {
            throw new NotImplementedException();
        }

        public bool RemoveSubordinate(Person subordinate)
        {
            throw new NotImplementedException();
        }

        public IReadOnlyList<Person> GetSubordinates()
        {
            throw new NotImplementedException();
        }


        public string Name
        {
            get { return _name; }
       
        }

        public string LastName
        {
            get { return _lastName; }
           
        }

        public int GetSalary()
        {
            return _salary + GetBonus();
        }

        public void SetSalary(int salary)
        {
            throw new NotImplementedException();
        }
        protected virtual int GetBonus()
        {
            return 0;
        }

        public virtual char GetSymbol()
        {
            throw new NotImplementedException();
        }
        public Person Boss
        {
            get { return _boss; }
            set { _boss = value; }
        }
    }
}
